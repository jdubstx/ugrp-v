﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ugrpv.Data.Interface;

namespace ugrpv.Account
{
    public class Account : IAccountData
    {
        public Account() { }

        public int sqlid { get; set; }
        
        public string user { get; set; }
        public string password { get; set; }

        public string email { get; set; }
        public string registeredon { get; set; }

        public string lastseen { get; set; }
        public string ip { get; set; }

        public int defcar { get; set; }
        public int suspend { get; set; }
        public int suspendtype { get; set; }

        public bool online { get; set; }
        //public virtual ICollection<Character> Character { get; set; }

        //public virtual ICollection<Ban> Ban { get; set; }
    }
}
