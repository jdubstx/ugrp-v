﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GTANetworkServer;
using ugrpv.Data.Interface;
using ugrpv.Database;
using GTANetworkShared;
namespace ugrpv.User
{
    public class AccountManager 
    {
        public string AccountId;
        public IAccountData Account;

        public int PlayerId;

        public Client Client { get; private set; }
        //public object AccountManager { get; internal set; }

        //public CharacterManager CharacterManager;

        public AccountManager(Account.Account account, Client client)
        {
            Account = account;
            client.setData("ACCOUNT", this);

            API.shared.sendNotificationToPlayer(client, string.Format("~w~Welcome {0} to {1}. Your last login was on {2}",
            account.user, Global.GlobalVars.ServerName, account.lastseen));

            account.lastseen = Convert.ToString(DateTime.Now);
            account.online = true;
        }

        public static bool IsAccountBanned(Client player)
        {
            return false;
        }
        public static bool IsAccountBanned(Account.Account param)
        {
            return false;
        }
        public static string LoadAccountPassword(Client player, string username)
        {
            string query = String.Format("SELECT password FROM user WHERE socialid='{0}'", username);
            DataTable result = DatabaseManager.Query(query);
            if (result.Rows.Count != 0)
            {
                foreach (DataRow row in result.Rows)
                { 
                    string password = Convert.ToString(row["password"]);
                    return password;
                }
            }
            return "";
        }
        public static bool LoadAccount(Client player, string username)
        {
            Account.Account account = new Account.Account();

            string query = String.Format("SELECT sqlid, socialid, password, email, registeredon, lastseen, defchar, suspend, suspendtype FROM user WHERE socialid='{0}'", username);
            DataTable result = DatabaseManager.Query(query);
            if (result.Rows.Count != 0)
            {
                foreach (DataRow row in result.Rows)
                {
                    account.sqlid = Convert.ToInt32(row["sqlid"]);
                    account.user = Convert.ToString(row["socialid"]);
                    account.password = Convert.ToString(row["password"]);
                    account.email = Convert.ToString(row["email"]);
                    account.registeredon = Convert.ToString(row["registeredon"]);
                    account.lastseen = Convert.ToString(row["lastseen"]);
                    account.ip = player.address;
                    account.defcar = Convert.ToInt32(row["defchar"]);
                    account.suspend = Convert.ToInt32(row["suspend"]);
                    account.suspendtype = Convert.ToInt32(row["suspendtype"]);
                    new AccountManager(account, player);
                }
                return true;
            }
            return false;
        }
        public static bool RegisterAccount(Client player, string email, string password)
        {
            string hash = sha256(password);
            if (!DoesAccountExist(player.socialClubName))
            {
                string query = string.Format("INSERT INTO user (socialid, password, email, registeredon, lastseen, ip, defchar, suspend, suspendtype) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}, {8})",
                player.socialClubName, hash, email, DateTime.Now, DateTime.Now, player.address, 0, 0, 0);
                DatabaseManager.Query(query);
                API.shared.consoleOutput(query);
                return true;
            }
            else return false;
        }
        public static bool DoesAccountExist(string username)
        {
            string query = String.Format("SELECT socialid='{0}' FROM user WHERE socialid='{0}'", username);
            DataTable result = DatabaseManager.Query(query);
            if (result != null && result.Rows.Count != 0) return true;
            else return false;
        }
        public static Client GetClientFromSocialClub(string SocialClub)
        {
            foreach (var client in API.shared.getAllPlayers())
            {
                if (client.socialClubName == SocialClub) return client;
            }
            return null;
        }
        public static string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(password), 0, Encoding.ASCII.GetByteCount(password));
            foreach (byte theByte in crypto)
                hash += theByte.ToString("x2");
            return hash;
        }
        public static bool Authenticate(Client player, string username, string password)
        {
            string query = String.Format("SELECT password FROM user WHERE socialid='{0}'", username);
            DataTable result = DatabaseManager.Query(query);
            if(result != null)
            {
                foreach (DataRow row in result.Rows)
                {
                    string dbPass = Convert.ToString(row["password"]);
                    string Hash = sha256(password);
                    if (string.Equals(Hash, dbPass))
                    {
                        return true;
                    }
                }   
            }
            return false;
        }
        public static void LoginAccount(Client player, string password)
        {
            
            if (API.shared.getEntityData(player, "ACCOUNT") != null)
            {
                API.shared.sendChatMessageToPlayer(player, "~r~SERVER: ~w~You're already logged in!");
                return;
            }
            if(Authenticate(player, player.socialClubName, password) == true)
            {
                LoadAccount(player, player.socialClubName);
                API.shared.sendNotificationToPlayer(player, "You have successfully logged in.", true);
                TempPlayerSPawn(player);
            }
            else
            {
                API.shared.sendChatMessageToPlayer(player, "~r~ERROR:~w~ Wrong password, or account doesnt exist!");
            }
        }
        public static void TempPlayerSPawn(Client player)
        {
            var DEFAULT_SKIN = API.shared.pedNameToModel("BevHills01AMY");
            API.shared.setPlayerSkin(player, DEFAULT_SKIN);
            API.shared.setEntityPosition(player, new Vector3(440.92f, -981.76f, 30.69f));
            API.shared.setEntityDimension(player, 0);
            player.freeze(false);
        }
    }
}

