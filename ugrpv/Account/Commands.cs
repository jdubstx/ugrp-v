﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;
using ugrpv.User;

namespace ugrpv.Account
{
    class Commands : Script
    {
        [Command("register", GreedyArg = true)]
        public void register(Client player, string email, string password)
        {
            if (AccountManager.RegisterAccount(player, email, password) == true)
            {
                API.sendChatMessageToPlayer(player, "Account Registered.");
            }
            else
            {
                API.sendChatMessageToPlayer(player, "ERROR: This account is already registered.");
            }
        }
        [Command("login", GreedyArg = true)]
        public void login(Client player, string password)
        {
            AccountManager.LoginAccount(player, password);
        }
        [Command("stats")]
        public void accountStats(Client player)
        {
            AccountManager account = player.getData("ACCOUNT");
            API.sendChatMessageToPlayer(player, "Account Statistics");
            API.sendChatMessageToPlayer(player, "------------------");
            API.sendChatMessageToPlayer(player, String.Format("SocialID: {0} Email: {1}", account.Account.user, account.Account.email));
            API.sendChatMessageToPlayer(player, String.Format("Register Date: {0} IP: {1}", account.Account.registeredon, account.Account.ip));
            API.sendChatMessageToPlayer(player, String.Format("Last Seen: {0}", account.Account.lastseen));

        }
    }
}
