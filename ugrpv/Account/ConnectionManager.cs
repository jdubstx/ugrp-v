﻿using GTANetworkServer;
using GTANetworkShared;
using System;

using System.Linq;
using System.Text;

using ugrpv.User;

namespace ugrpv
{
    class ConnectionManager : Script
    {
        public static readonly Vector3 _startPos = new Vector3(3433.339f, 5177.579f, 39.79541f);
        public static readonly Vector3 _startCamPos = new Vector3(3476.85f, 5228.022f, 9.453369f);

        public ConnectionManager()
        {
            API.onPlayerConnected += OnPlayerConnected;
            API.onPlayerFinishedDownload += OnPlayerFinishedDownload;
            API.onPlayerDisconnected += OnPlayerDisconnected;
        }
        public void OnPlayerConnected(Client player)
        {
            if(AccountManager.IsAccountBanned(player))
            {
                //player.kick("~r~You are banned from this server.");
            }
        }
        public void OnPlayerFinishedDownload(Client player)
        {
            API.setEntityData(player, "STATE_DOWNLOAD_FINISHED", true);
            API.sendChatMessageToPlayer(player, "Welcome to Underground Roleplay. Please /login or /register.");
            API.sendChatMessageToPlayer(player, "UGRP V");
            LoginMenu(player);
            //Login
        }
        public void OnPlayerDisconnected(Client player, string reason)
        {
            AccountManager account = player.getData("ACCOUNT");
            if (account == null) return;
            LogOut(account);
        }
        public static void LogOut(AccountManager account, int type = 0)
        {
            account.Account.online = false;
            if(type != 0)
            {
                LoginMenu(account.Client);
            }
        }
        public static void LoginMenu(Client player)
        {
            API.shared.triggerClientEvent(player, "interpolateCamera", 20000, _startCamPos, _startCamPos + new Vector3(0.0, -50.0, 50.0), new Vector3(0.0, 0.0, 180.0), new Vector3(0.0, 0.0, 95.0));
            player.position = _startPos;
            player.freeze(true);
            player.transparency = 0;
        }

        
    }


}
















