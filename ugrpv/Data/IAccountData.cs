﻿using System;
using System.Collections.Generic;

namespace ugrpv.Data.Interface
{
    public interface IAccountData
    {
        int sqlid { get; set; }

        string user { get; set; }

        string password { get; set; }

        string email { get; set; }

        string ip { get; set; }

        int defcar { get; set; }

        int suspend { get; set; }

        int suspendtype { get; set; }

        string registeredon { get; set; }

        string lastseen { get; set; }

        bool online { get; set; }

        //ICollection<Character> Character { get; set; }

        //ICollection<Ban> Ban { get; set; }
    }
}
