﻿using System;
using System.Data;
using GTANetworkServer;
using GTANetworkShared;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace ugrpv.Database
{
    class DatabaseManager : Script
    {
        public DatabaseManager()
        {
            API.onResourceStart += OnResourceStart;
            API.onResourceStop += OnResourceStop;
        }
        public void OnResourceStart()
        {
            API.consoleOutput("Database Module Started.");
        }
        public void OnResourceStop()
        {

        }
        public static void GetServerType()
        {
            switch(Global.GlobalVars.ServerType)
            {
                case 1:
                    {
                        Global.GlobalVars.MySQLConnect = @"server=localhost;userid=ugrp; password=ugrppw1;database=ugrp_gtan;";
                        break;
                    }
                case 2:
                    {
                        Global.GlobalVars.MySQLConnect = @"server=localhost;userid=ugrp; password=ugrppw1;database=ugrp_gtan;";
                        break;
                    }
            }
        }

        static public DataTable Query(string sql)
        {
            string connStr = @"server=localhost;userid=ugrp;
            password=ugrppw1;database=ugrp_gtan;";
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                conn.Open();
                try
                {
                    MySqlCommand cmd = new MySqlCommand(sql, conn);
                    Console.WriteLine(String.Format("db.query: {0}", sql));
                    MySqlDataReader rdr = cmd.ExecuteReader();
                    DataTable results = new DataTable();
                    results.Load(rdr);
                    rdr.Close();
                    conn.Close();
                    return results;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("db.ERROR: " + ex.ToString());
                    conn.Close();
                    return null;
                }

            }
        }
    }
}
