﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTANetworkServer;
using GTANetworkShared;

namespace ugrpv.Global
{
    class Commands : Script
    {
        [Command("colors")]
        public void GameVehicleColorsCommand(Client sender, int primaryColor, int secondaryColor)
        {
            if (!sender.vehicle.IsNull)
            {
                API.setVehiclePrimaryColor(sender.vehicle, primaryColor);
                API.setVehicleSecondaryColor(sender.vehicle, secondaryColor);
                API.sendChatMessageToPlayer(sender, "Colors applied successfully!");
            }
            else
            {
                API.sendChatMessageToPlayer(sender, "~r~ERROR: ~w~You're not in a vehicle!");
            }
        }
        [Command("veh")]
        public void SpawnCarCommand(Client sender, string modelName)
        {
            var rot = API.getEntityRotation(sender.handle);
            var veh = API.createVehicle(API.vehicleNameToModel(modelName), sender.position, new Vector3(0, 0, rot.Z), 0, 0);
            API.setPlayerIntoVehicle(sender, veh, -1);
        }


    }
}
