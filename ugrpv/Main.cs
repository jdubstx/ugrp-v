﻿using GTANetworkServer;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace ugrpv
{
    class Main : Script
    {
        public Main()
        {
            API.onResourceStart += OnResourceStart;
            API.onResourceStop += OnResourceStop;
        }
        private void OnResourceStart()
        {
            API.consoleOutput(Global.GlobalVars.ServerName + " was started at " + DateTime.Now);
        }
        private void OnResourceStop()
        {

        }
    }
}
